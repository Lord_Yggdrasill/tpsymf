<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{

    /**
     * @Route("/home", name="home", methods={"GET"})
     */
    public function home()
    {
        return $this->render("front/home.html.twig");
    }

    /**
     * @Route("/login", name="login", methods={"GET", "POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError(); //Retourne erreur si mauvaise authentification
        $lastUsername = $authenticationUtils->getLastUsername(); //Retourne le dernier username posté
        return $this->render("front/login.html.twig", [
            'error' => $error,
            'last_username' => $lastUsername,
        ]);
    }
}