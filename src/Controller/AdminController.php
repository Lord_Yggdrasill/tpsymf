<?php

// Composercat : pour les fainéants de la ligne de commande

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="adminIndex", methods={"GET"})
     */
    public function index()
    {
        return $this->render("admin/index.html.twig", []);
    }
}