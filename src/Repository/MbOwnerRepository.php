<?php

namespace App\Repository;

use App\Entity\MbOwner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MbOwner|null find($id, $lockMode = null, $lockVersion = null)
 * @method MbOwner|null findOneBy(array $criteria, array $orderBy = null)
 * @method MbOwner[]    findAll()
 * @method MbOwner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MbOwnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MbOwner::class);
    }

    // /**
    //  * @return MbOwner[] Returns an array of MbOwner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MbOwner
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
