<?php

namespace App\Repository;

use App\Entity\CustomerRent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomerRent|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerRent|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerRent[]    findAll()
 * @method CustomerRent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerRent::class);
    }

    // /**
    //  * @return CustomerRent[] Returns an array of CustomerRent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerRent
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
