<?php

namespace App\Repository;

use App\Entity\BillRow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BillRow|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillRow|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillRow[]    findAll()
 * @method BillRow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillRowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BillRow::class);
    }

    // /**
    //  * @return BillRow[] Returns an array of BillRow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BillRow
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
