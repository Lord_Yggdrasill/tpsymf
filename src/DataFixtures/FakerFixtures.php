<?php


namespace App\DataFixtures;

use App\Entity\Bill;
use App\Entity\Customer;
use App\Entity\CustomerRent;
use App\Entity\MbOwner;
use App\Entity\Rent;
use App\Entity\Season;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class FakerFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        // Création de données en langue FR
        $faker = Faker\Factory::create('fr_FR');
        $faker->seed('flo');

        // je crée 10 users avec le faker de fzaninotto
        for ($i = 0; $i < 10; $i++) {
            $ref = 3;
            $users = new Users();
            $users->setName($faker->lastName);
            $users->setFirstname($faker->firstName);
            $users->setEmail($faker->email);
            $users->setUsername($faker->userName);
            //encodage du password "admin"
            $users->setPassword($this->encoder->encodePassword($users, "admin"));
            $users->setRoleId($faker->numberBetween(1, 3));

            $manager->persist($users);
            //J'ajoute une référence de chaque user pour la foreign key
            $this->addReference('user-'.$i, $users);
        }

        //création de 50 mobil-home
        for ($i = 1; $i < 51; $i++) {
            $rent = new Rent();
            $rent->setLocationId($i);
            $rent->setLocationSize(0);
            // array avec les 3 types de logements, puis fake avec "randomElement" pour prendre un des
            // 3 types au hasard
//            $type = ['caravane', 'mobil-home', 'emplacement'];
//            $rent->setType($faker->randomElement($type));
            $rent->setType('mobil-home');
            //array pour setter une valeur au hasard parmi celui-ci
            $capacity = [3, 4, 5, 6];
            $k_capacity = array_rand($capacity);
            $v_capacity = $capacity[$k_capacity];
            $rent->setCapacity($v_capacity);
            $rent->setImg('images/mobil-home.jpg');
            $rent->setIsAvailable($faker->boolean);
            $rent->setIsPrivate($faker->boolean(60));
            $rent->setDescription($faker->sentence(15, true));
            //array pour setter une valeur au hasard parmi celui-ci
            $price = [20, 24, 27, 34];
            $k_price = array_rand($price);
            $v_price = $price[$k_price];
            $rent->setPrice($v_price);

            $this->addReference('rent-'.$i, $rent);

            $manager->persist($rent);
        }

        //création de 10 caravanes
        for ($i = 51; $i < 61; $i++) {
            $rent = new Rent();
            $rent->setLocationId($i);
            $rent->setLocationSize(0);
            // array avec les 3 types de logements, puis fake avec "randomElement" pour prendre un des
            // 3 types au hasard
//            $type = ['caravane', 'mobil-home', 'emplacement'];
//            $rent->setType($faker->randomElement($type));
            $rent->setType('caravane');
            //array pour setter une valeur au hasard parmi celui-ci
            $capacity = [2, 4, 6];
            $k_capacity = array_rand($capacity);
            $v_capacity = $capacity[$k_capacity];
            $rent->setCapacity($v_capacity);
            $rent->setImg('images/caravane.jpg');
            $rent->setIsAvailable($faker->boolean);
            $rent->setIsPrivate(0);
            $rent->setDescription($faker->sentence(15, true));
            //array pour setter une valeur au hasard parmi celui-ci
            $price = [15, 18, 24];
            $k_price = array_rand($price);
            $v_price = $price[$k_price];
            $rent->setPrice($v_price);

            $this->addReference('rent-'.$i, $rent);

            $manager->persist($rent);
        }


        //création de 30 emplacements
        for ($i = 61; $i < 91; $i++) {
            $rent = new Rent();
            $rent->setLocationId($i);
            //array pour setter une valeur au hasard parmi celui-ci
            $size = [8, 12];
            $k_size = array_rand($size);
            $v_size = $size[$k_size];
            $rent->setLocationSize($v_size);
            // array avec les 3 types de logements, puis fake avec "randomElement" pour prendre un des
            // 3 types au hasard
//            $type = ['caravane', 'mobil-home', 'emplacement'];
//            $rent->setType($faker->randomElement($type));
            $rent->setType('emplacement');
            $rent->setCapacity(0);
            $rent->setImg('images/emplacement.jpg');
            $rent->setIsAvailable($faker->boolean);
            $rent->setIsPrivate(0);
            $rent->setDescription($faker->sentence(15, true));
            //array pour setter une valeur au hasard parmi celui-ci
            $price = [12, 14];
            $k_price = array_rand($price);
            $v_price = $price[$k_price];
            $rent->setPrice($v_price);

            $this->addReference('rent-'.$i, $rent);

            $manager->persist($rent);
        }

        //Création de la saison
        $season = new Season();
//            $season->getCustomerRentId();
        $season->setBeginning($faker->dateTime('2000-01-01'));
        $season->setEnding($faker->dateTime('2005-05-05'));

//        $this->addReference('season', $season);

        $manager->persist($season);

        //Je crée 20 locations
        for ($i = 0; $i < 8; $i++) {
            $customer_rent = new CustomerRent();
            $customer_rent->setCheckin($faker->dateTimeBetween('2020-05-05', '2020-10-10'));
            $checkin = $customer_rent->getCheckin();
            $checkout = $faker->dateTimeBetween($checkin, '2020-10-10');
            $customer_rent->setCheckout($checkout);
            $customer_rent->setNbAdult($faker->numberBetween(1, 15));
            $customer_rent->setNbChild($faker->numberBetween(0, 20));
            $customer_rent->setSpDaysAdult($faker->numberBetween(0, 7));
            $customer_rent->setSpDaysChild($faker->numberBetween(0, 7));
            $customer_rent->setSeason($season);

            $manager->persist($customer_rent);

            $this->addReference('customer_rent-'.$i, $customer_rent);
            $this->addReference('checkout-'.$i, $customer_rent->getCheckout());
//            $this->getReference('season');
        }


        //Je crée 8 customers
        for ($i = 0; $i < 8; $i++) {
            $customer = new Customer();
            $customer->setUserId($this->getReference('user-'.$i));
            $customer->setCustomerRentId($this->getReference('customer_rent-'.$i));
            $customer->setAddress($faker->address);
            $customer->setPhone($faker->phoneNumber);
            $this->addReference('customer-'.$i, $customer);

            $manager->persist($customer);


        }

        //Je crée 8 factures
        for ($i = 0; $i < 8; $i++) {
            $bill_number = 1 + $i;
//            dump($this->getReference('customer-' . $i));
            $bill = new Bill();
            $bill->setCustomerId($this->getReference('customer-'.$i));
            $bill->setNumber('Facture ' . $faker->year . ' - ' . $faker->month . ' - ' . $bill_number);
            $bill->setDate($customer_rent->getCheckout());

            $manager->persist($bill);
        }

        //Je crée 30 propriétaires
        for ($i = 0; $i < 30; $i++) {
            $owner = new MbOwner();
            $owner->addCustomerId($this->getReference('customer-'.rand(0, 7)));
            $owner->addRentId($this->getReference('rent-'.rand(1, 90)));
            $owner->setRetribution($faker->numberBetween(20, 500));

            $manager->persist($owner);
        }


        $manager->flush();
    }
}