<?php

namespace App\Entity;

use App\Repository\CustomerRentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRentRepository::class)
 */
class CustomerRent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $checkin;

    /**
     * @ORM\Column(type="date")
     */
    private $checkout;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_adult;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_child;

    /**
     * @ORM\Column(type="integer")
     */
    private $Sp_days_adult;

    /**
     * @ORM\Column(type="integer")
     */
    private $Sp_days_child;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class, inversedBy="customer_rent_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $season;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCheckin(): ?\DateTimeInterface
    {
        return $this->checkin;
    }

    public function setCheckin(\DateTimeInterface $checkin): self
    {
        $this->checkin = $checkin;

        return $this;
    }

    public function getCheckout(): ?\DateTimeInterface
    {
        return $this->checkout;
    }

    public function setCheckout(\DateTimeInterface $checkout): self
    {
        $this->checkout = $checkout;

        return $this;
    }

    public function getNbAdult(): ?int
    {
        return $this->nb_adult;
    }

    public function setNbAdult(int $nb_adult): self
    {
        $this->nb_adult = $nb_adult;

        return $this;
    }

    public function getNbChild(): ?int
    {
        return $this->nb_child;
    }

    public function setNbChild(int $nb_child): self
    {
        $this->nb_child = $nb_child;

        return $this;
    }

    public function getSpDaysAdult(): ?int
    {
        return $this->Sp_days_adult;
    }

    public function setSpDaysAdult(int $Sp_days_adult): self
    {
        $this->Sp_days_adult = $Sp_days_adult;

        return $this;
    }

    public function getSpDaysChild(): ?int
    {
        return $this->Sp_days_child;
    }

    public function setSpDaysChild(int $Sp_days_child): self
    {
        $this->Sp_days_child = $Sp_days_child;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setSeason(?Season $season): self
    {
        $this->season = $season;

        return $this;
    }
}
