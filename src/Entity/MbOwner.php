<?php

namespace App\Entity;

use App\Repository\MbOwnerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MbOwnerRepository::class)
 */
class MbOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Rent::class, mappedBy="mbOwner")
     */
    private $rent_id;

    /**
     * @ORM\OneToMany(targetEntity=Customer::class, mappedBy="mbOwner")
     */
    private $customer_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $retribution;

    public function __construct()
    {
        $this->rent_id = new ArrayCollection();
        $this->customer_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Rent[]
     */
    public function getRentId(): Collection
    {
        return $this->rent_id;
    }

    public function addRentId(Rent $rentId): self
    {
        if (!$this->rent_id->contains($rentId)) {
            $this->rent_id[] = $rentId;
            $rentId->setMbOwner($this);
        }

        return $this;
    }

    public function removeRentId(Rent $rentId): self
    {
        if ($this->rent_id->contains($rentId)) {
            $this->rent_id->removeElement($rentId);
            // set the owning side to null (unless already changed)
            if ($rentId->getMbOwner() === $this) {
                $rentId->setMbOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getCustomerId(): Collection
    {
        return $this->customer_id;
    }

    public function addCustomerId(Customer $customerId): self
    {
        if (!$this->customer_id->contains($customerId)) {
            $this->customer_id[] = $customerId;
            $customerId->setMbOwner($this);
        }

        return $this;
    }

    public function removeCustomerId(Customer $customerId): self
    {
        if ($this->customer_id->contains($customerId)) {
            $this->customer_id->removeElement($customerId);
            // set the owning side to null (unless already changed)
            if ($customerId->getMbOwner() === $this) {
                $customerId->setMbOwner(null);
            }
        }

        return $this;
    }

    public function getRetribution(): ?int
    {
        return $this->retribution;
    }

    public function setRetribution(int $retribution): self
    {
        $this->retribution = $retribution;

        return $this;
    }
}
