<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Users::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity=MbOwner::class, inversedBy="customer_id")
     */
    private $mbOwner;

    /**
     * @ORM\OneToOne(targetEntity=CustomerRent::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer_rent_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?Users
    {
        return $this->user_id;
    }

    public function setUserId(Users $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMbOwner(): ?MbOwner
    {
        return $this->mbOwner;
    }

    public function setMbOwner(?MbOwner $mbOwner): self
    {
        $this->mbOwner = $mbOwner;

        return $this;
    }

    public function getCustomerRentId(): ?CustomerRent
    {
        return $this->customer_rent_id;
    }

    public function setCustomerRentId(CustomerRent $customer_rent_id): self
    {
        $this->customer_rent_id = $customer_rent_id;

        return $this;
    }
}
