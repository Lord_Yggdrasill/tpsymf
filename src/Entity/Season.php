<?php

namespace App\Entity;

use App\Repository\SeasonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeasonRepository::class)
 */
class Season
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=CustomerRent::class, mappedBy="season")
     */
    private $customer_rent_id;

    /**
     * @ORM\Column(type="date")
     */
    private $beginning;

    /**
     * @ORM\Column(type="date")
     */
    private $ending;

    public function __construct()
    {
        $this->customer_rent_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|CustomerRent[]
     */
    public function getCustomerRentId(): Collection
    {
        return $this->customer_rent_id;
    }

    public function addCustomerRentId(CustomerRent $customerRentId): self
    {
        if (!$this->customer_rent_id->contains($customerRentId)) {
            $this->customer_rent_id[] = $customerRentId;
            $customerRentId->setSeason($this);
        }

        return $this;
    }

    public function removeCustomerRentId(CustomerRent $customerRentId): self
    {
        if ($this->customer_rent_id->contains($customerRentId)) {
            $this->customer_rent_id->removeElement($customerRentId);
            // set the owning side to null (unless already changed)
            if ($customerRentId->getSeason() === $this) {
                $customerRentId->setSeason(null);
            }
        }

        return $this;
    }

    public function getBeginning(): ?\DateTimeInterface
    {
        return $this->beginning;
    }

    public function setBeginning(\DateTimeInterface $beginning): self
    {
        $this->beginning = $beginning;

        return $this;
    }

    public function getEnding(): ?\DateTimeInterface
    {
        return $this->ending;
    }

    public function setEnding(\DateTimeInterface $ending): self
    {
        $this->ending = $ending;

        return $this;
    }
}
