<?php

namespace App\Entity;

use App\Repository\TaxesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaxesRepository::class)
 */
class Taxes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $adult_tax;

    /**
     * @ORM\Column(type="integer")
     */
    private $child_tax;

    /**
     * @ORM\Column(type="integer")
     */
    private $Sp_adult;

    /**
     * @ORM\Column(type="integer")
     */
    private $Sp_child;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdultTax(): ?int
    {
        return $this->adult_tax;
    }

    public function setAdultTax(int $adult_tax): self
    {
        $this->adult_tax = $adult_tax;

        return $this;
    }

    public function getChildTax(): ?int
    {
        return $this->child_tax;
    }

    public function setChildTax(int $child_tax): self
    {
        $this->child_tax = $child_tax;

        return $this;
    }

    public function getSpAdult(): ?int
    {
        return $this->Sp_adult;
    }

    public function setSpAdult(int $Sp_adult): self
    {
        $this->Sp_adult = $Sp_adult;

        return $this;
    }

    public function getSpChild(): ?int
    {
        return $this->Sp_child;
    }

    public function setSpChild(int $Sp_child): self
    {
        $this->Sp_child = $Sp_child;

        return $this;
    }
}
