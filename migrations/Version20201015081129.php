<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015081129 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bill (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, customer_id_id INTEGER NOT NULL, number VARCHAR(255) NOT NULL, date DATETIME NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7A2119E3B171EB6C ON bill (customer_id_id)');
        $this->addSql('CREATE TABLE bill_row (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bill_id_id INTEGER NOT NULL, label VARCHAR(255) NOT NULL, quantity INTEGER NOT NULL, unit_price INTEGER NOT NULL, discount INTEGER NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_222914C949B4CBC9 ON bill_row (bill_id_id)');
        $this->addSql('CREATE TABLE customer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id_id INTEGER NOT NULL, mb_owner_id INTEGER DEFAULT NULL, customer_rent_id_id INTEGER NOT NULL, address CLOB NOT NULL, phone VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E099D86650F ON customer (user_id_id)');
        $this->addSql('CREATE INDEX IDX_81398E0993208EA9 ON customer (mb_owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E0963F36863 ON customer (customer_rent_id_id)');
        $this->addSql('CREATE TABLE customer_rent (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, season_id INTEGER NOT NULL, checkin DATE NOT NULL, checkout DATE NOT NULL, nb_adult INTEGER NOT NULL, nb_child INTEGER NOT NULL, sp_days_adult INTEGER NOT NULL, sp_days_child INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_56E9E9BB4EC001D1 ON customer_rent (season_id)');
        $this->addSql('CREATE TABLE mb_owner (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, retribution INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE rent (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, mb_owner_id INTEGER DEFAULT NULL, location_id INTEGER NOT NULL, location_size INTEGER NOT NULL, type VARCHAR(255) NOT NULL, capacity INTEGER NOT NULL, img VARCHAR(255) DEFAULT NULL, is_available BOOLEAN NOT NULL, is_private BOOLEAN NOT NULL, price INTEGER NOT NULL, description CLOB NOT NULL)');
        $this->addSql('CREATE INDEX IDX_2784DCC93208EA9 ON rent (mb_owner_id)');
        $this->addSql('CREATE TABLE season (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, beginning DATE NOT NULL, ending DATE NOT NULL)');
        $this->addSql('CREATE TABLE taxes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, adult_tax INTEGER NOT NULL, child_tax INTEGER NOT NULL, sp_adult INTEGER NOT NULL, sp_child INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, role_id INTEGER NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bill');
        $this->addSql('DROP TABLE bill_row');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE customer_rent');
        $this->addSql('DROP TABLE mb_owner');
        $this->addSql('DROP TABLE rent');
        $this->addSql('DROP TABLE season');
        $this->addSql('DROP TABLE taxes');
        $this->addSql('DROP TABLE users');
    }
}
