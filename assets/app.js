/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
 import $ from 'jquery';
 //Je peux importer le bootstrap d'ici, ou directement dans "app.css"
 import 'bootstrap/dist/css/bootstrap.css';

// const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything

//Ajout de bootstrap
require('bootstrap');

//Ajout de font-awesome
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

//fonction activation des popovers
$(document).ready(function (){
 $('[data-toggle="popover"]').popover();
})

console.log('Hello Webpack Encore! Edit me in assets/app.js');
